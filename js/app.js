const API_URL = `${document.location.protocol}//${document.location.hostname}/api`;

let listPlaces = [];
let myListPlaces = [];

getData = async () => {
  setListPlaces();
  setMyListPlaces();
  setListCity(await apiFetch("/city"));
  setListType(await apiFetch("/type"));
};

setListPlaces = async (city = "", type = "", min = "", max = "") => {
  let listPlaces = await apiFetch();
  let element = document.getElementById("resultados");
  element.innerHTML = "";
  listPlaces.forEach((e) => {
    e.new_price = e.price.replace(",", "").replace("$", "");

    if (
      (city.length == 0 || city == e.city) &&
      (type.length == 0 || type == e.type) &&
      (min.length == 0 || (e.new_price >= min && e.new_price <= max))
    ) {
      let el = htmlToElement(`
        <div
            class="itemMostrado my-2"
            >
            <img src="img/home.jpg" alt=""/>
              <div class="card-stacked">
                <p class="m-0 pl-2"><strong>Dirección:</strong> ${e.address}</p>
                <p class="m-0 pl-2"><strong>Ciudad:</strong> ${e.city}</p>
                <p class="m-0 pl-2"><strong>Teléfono:</strong> ${e.phone}</p>
                <p class="m-0 pl-2"><strong>código postal:</strong> ${
                  e.code
                }</p>
                <p class="m-0 pl-2"><strong>Tipo:</strong> ${e.type}</p>
                <p class="pl-2 precioTexto"><strong>Precio:</strong> ${
                  e.price
                }</p>
                ${
                  e.is_favorite == 1
                    ? ""
                    : `<button class="ml-2 btn btn-success">Guardar</button>`
                }
            </div>
        </div>`);

      let btn = el.querySelector(".btn-success");
      if (btn) {
        btn.addEventListener("click", async (_this) => {
          _this.target.parentNode.removeChild(_this.target);

          await apiFetch("/place", "post", {
            id: e.id,
          });
          setMyListPlaces();
        });
      }
      element.append(el);
    }
  });
};

setMyListPlaces = async (city = "", type = "") => {
  let listPlaces = await apiFetch("/favorite");
  let element = document.getElementById("misResultados");
  element.innerHTML = "";

  listPlaces.forEach((e) => {
    if (
      (city.length == 0 || city == e.city) &&
      (type.length == 0 || type == e.type)
    ) {
      let el = htmlToElement(`
        <div
            class="itemMostrado"
            >
            <img src="img/home.jpg" alt=""/>
            <div class="card-stacked">
                <p class="m-0 pl-2"><strong>Dirección:</strong> ${e.address}</p>
                <p class="m-0 pl-2"><strong>Ciudad:</strong> ${e.city}</p>
                <p class="m-0 pl-2"><strong>Teléfono:</strong> ${e.phone}</p>
                <p class="m-0 pl-2"><strong>código postal:</strong> ${e.code}</p>
                <p class="m-0 pl-2"><strong>Tipo:</strong> ${e.type}</p>
                <p class="precioTexto pl-2"><strong>Precio:</strong> ${e.price}</p>
                <button class="ml-2 btn btn-success">Eliminar</button>
            </div>
        </div>`);
      let btn = el.querySelector(".btn-success");
      if (btn) {
        btn.addEventListener("click", async (_this) => {
          await apiFetch("/place", "post", {
            id: e.id,
          });
          setMyListPlaces();
          setListPlaces();
        });
      }

      element.append(el);
    }
  });
};

setListCity = (citys = []) => {
  let element = document.getElementById("selectCiudad");
  citys.forEach((e) => {
    let _e = document.createElement("option");
    _e.value = e.city;
    _e.textContent = e.city;

    element.append(_e);
  });
};

setListType = (types = []) => {
  let element = document.getElementById("selectTipo");
  types.forEach((e) => {
    let _e = document.createElement("option");
    _e.value = e.type;
    _e.textContent = e.type;

    element.append(_e);
  });
};

apiFetch = async (endpoint = "", method = "get", data = []) => {
  switch (method) {
    case "get":
      try {
        return await fetch(`${API_URL}${endpoint}`)
          .then((res) => res.json())
          .then((res) => res.data);
      } catch (error) {}

      break;

    case "post":
      try {
        return await fetch(`${API_URL}${endpoint}`, {
          method: "POST",
          body: JSON.stringify(data),
        })
          .then((res) => res.json())
          .then((res) => res.data);
      } catch (error) {}

      break;

    case "delete":
      try {
        return await fetch(`${API_URL}${endpoint}`, {
          method: "DELETE",
          body: JSON.stringify(data),
        })
          .then((res) => res.json())
          .then((res) => res.data);
      } catch (error) {}

      break;

    default:
      break;
  }
};

document.getElementById("formulario").addEventListener("submit", async (e) => {
  e.preventDefault();

  let city = document.getElementById("selectCiudad").value;
  let type = document.getElementById("selectTipo").value;
  let range = document.getElementById("rangoPrecio").value;

  range = range.split(";");

  if (!(city.length == 0 && type.length == 0)) {
    setListPlaces(city, type, range[0], range[1]);
    setMyListPlaces(city, type, range[0], range[1]);
  } else {
    setListPlaces();
    setMyListPlaces();
  }
});

htmlToElement = (html) => {
  var template = document.createElement("template");
  html = html.trim();
  template.innerHTML = html;
  return template.content.firstChild;
};

getData();
