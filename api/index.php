<?php
header('Access-Control-Allow-Origin: *');
header("Access-Control-Allow-Headers: Origin, X-Requested-With, Content-Type, Accept");
header('Access-Control-Allow-Methods: GET, POST, PUT, DELETE');

header("Content-Type: application/json; charset=UTF-8");

require_once "./Helpers.php";
require_once "./Route.php";

require_once "./Connection.php";
require_once "./Place.php";



define('BASEPATH', '/api');

Route::get('/', function () {
    $data = (new Place())::all();

    response([
        'status' => 200,
        'message' => "Ok",
        'data' => $data,
    ]);
});

Route::get('/city', function () {
    $place = new Place();
    $data = $place::all();

    $place = new Place();
    $data = $place->query("SELECT DISTINCT city FROM places ORDER BY 1");

    response([
        'status' => 200,
        'message' => "Ok",
        'data' => $data,
    ]);
});

Route::get('/type', function () {
    $place = new Place();
    $data = $place->query("SELECT DISTINCT `type` FROM places ORDER BY 1");

    response([
        'status' => 200,
        'message' => "Ok",
        'data' => $data,
    ]);
});

Route::post('/place', function () {
    $place = new Place();

    $json = file_get_contents('php://input');
    $request = json_decode($json);

    $obj = (object)$place::find($request->id);

    $obj->is_favorite = $obj->is_favorite == 1 ? 0 : 1;
    $sql = "UPDATE places SET is_favorite = " . $obj->is_favorite . " WHERE id = $obj->id";
    $data = $place->query($sql);

    response([
        'status' => 200,
        'message' => $sql,
        'data' => $data,
    ]);
});

Route::get('/favorite', function () {
    $place = new Place();
    $data = $place->query("SELECT * FROM places WHERE is_favorite = 1");

    response([
        'status' => 200,
        'message' => "Ok",
        'data' => $data,
    ]);
});



Route::pathNotFound(function ($path) {
    header('HTTP/1.0 404 Not Found');
    response([
        'status' => 404,
        'message' => 'Method Not Found',
        'data' => [],
    ]);
});

Route::methodNotAllowed(function ($path, $method) {
    header('HTTP/1.0 405 Method Not Allowed');
    response([
        'status' => 405,
        'message' => 'Method Not Allowed',
        'data' => [],
    ]);
});

Route::run(BASEPATH);
