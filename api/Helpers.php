<?php

function dd($data)
{
    print_r('<pre>');
    print_r($data);
    print_r('</pre>');
    die();
}

function response($data)
{
    $response = json_encode($data);
    echo $response;
    die();
}
