<?php

class Connection
{
    private $type = "mysql";
    private $host = "localhost";
    private $user = "root";
    private $pass = "";
    private $db = "intelcost";
    public $conn;

    public function __construct()
    {
        try {
            $this->conn = new PDO("$this->type:host=$this->host;dbname=$this->db", $this->user, $this->pass, [
                PDO::ATTR_PERSISTENT => true
            ]);
        } catch (PDOException $e) {
            throw $e->getMessage();
        }
    }

    public function select($table, $id = '')
    {
        $sql = "SELECT * FROM $table ";

        if (!empty($id)) {
            $sql .= "WHERE id = $id";
        }

        return $this->query($sql);
    }

    public function query($sql)
    {
        $exec = $this->conn->prepare($sql);

        if ($exec->execute()) {
            return $exec->fetchAll(PDO::FETCH_ASSOC);
        }

        return [];
    }
}
