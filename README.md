# suplosBackEnd
Prueba suplos desarrollador backend

# Pasos para implementarlo

Crear el dominio en /etc/hosts

`127.0.0.1 suplos.local`

Tener instalado docker y docker-compose

descomprimir la carpeta

dirigirse a la carpeta docker

ejecutar los siguientes comandos

`cp .env.example .env`

`docker-compose up -d --build --remove-orphans --force-recreate`
